var express = require('express');
var router = express.Router();
var addresses_dal = require('../model/addresses_dal');


// View All addresses
router.get('/all', function(req, res) {
    addresses_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('addresses/addressesViewAll', { 'result':result });
        }
    });

});

// View the addresses for the given id
router.get('/', function(req, res){
    if(req.query.addresses_id == null) {
        res.send('addresses_id is null');
    }
    else {
        addresses_dal.getById(req.query.addresses_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('addresses/addressesViewById', {'result': result});
            }
        });
    }
});

// Return the add a new addresses form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    addresses_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('addresses/addressesAdd', {'addresses': result});
        }
    });
});

// View the addresses for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.street == null) {
        res.send('Address must be provided.');
    }
    else if(req.query.addresses_id == null) {
        res.send('At least one address must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        addresses_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/addresses/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.addresses_id == null) {
        res.send('An address id is required');
    }
    else {
        addresses_dal.edit(req.query.addresses_id, function(err, result){
            res.render('addresses/addressesUpdate', {addresses: result[0][0], addresses: result[1]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.addresses_id == null) {
        res.send('An address id is required');
    }
    else {
        addresses_dal.getById(req.query.addresses_id, function(err, addresses){
            addresses_dal.getAll(function(err, addresses) {
                res.render('addresses/addressesUpdate', {addresses: addresses[0], addresses: addresses});
            });
        });
    }

});

router.get('/update', function(req, res) {
    addresses_dal.update(req.query, function(err, result){
        res.redirect(302, '/addresses/all');
    });
});

// Delete a addresses for the given addresses_id
router.get('/delete', function(req, res){
    if(req.query.addresses_id == null) {
        res.send('addresses_id is null');
    }
    else {
        addresses_dal.delete(req.query.addresses_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/addresses/all');
            }
        });
    }
});

module.exports = router;
var express = require('express');
var router = express.Router();
var type_dal= require('../model/type_dal');


// View All types
router.get('/all', function(req, res) {
    type_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('type/typeViewAll', { 'result':result });
        }
    });

});

// View the type for the given id
router.get('/', function(req, res){
    if(req.query.type_id == null) {
        res.send('type_id is null');
    }
    else {
        type_dal.getById(req.query.type_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('type/typeViewById', {'result': result});
            }
        });
    }
});

// Return the add a new type form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    type_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('type/typeAdd', {'type': result});
        }
    });
});

// View the type for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.street == null) {
        res.send('Type must be provided.');
    }
    else if(req.query.type_id == null) {
        res.send('At least one type must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        type_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/type/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.type_id == null) {
        res.send('A type id is required');
    }
    else {
        type_dal.edit(req.query.type_id, function(err, result){
            res.render('type/typeUpdate', {type: result[0][0], type: result[1]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.type_id == null) {
        res.send('A type id is required');
    }
    else {
        type_dal.getById(req.query.type_id, function(err, type){
            type_dal.getAll(function(err, type) {
                res.render('type/typeUpdate', {type: type[0], type: type});
            });
        });
    }

});

router.get('/update', function(req, res) {
    type_dal.update(req.query, function(err, result){
        res.redirect(302, '/type/all');
    });
});

// Delete a type for the given type_id
router.get('/delete', function(req, res) {
    if (req.query.type_id == null) {
        res.send('type_id is null');
    }
    else {
        type_dal.delete(req.query.type_id, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/type/all');
            }
        });
    }
});


router.get('/high_value', function(req, res) {
    type_dal.high_value(function (err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('type/high_value', {'result': result});
        }
    });
});


module.exports = router;

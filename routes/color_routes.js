var express = require('express');
var router = express.Router();
var color_dal= require('../model/color_dal');


// View All colors
router.get('/all', function(req, res) {
    color_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('color/colorViewAll', { 'result':result });
        }
    });

});

// View the color for the given id
router.get('/', function(req, res){
    if(req.query.color_id == null) {
        res.send('color_id is null');
    }
    else {
        color_dal.getById(req.query.color_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('color/colorViewById', {'result': result});
            }
        });
    }
});

// Return the add a new color form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    color_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('color/colorAdd', {'color': result});
        }
    });
});

// View the color for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.street == null) {
        res.send('Color must be provided.');
    }
    else if(req.query.color_id == null) {
        res.send('At least one color must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        color_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/color/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.color_id == null) {
        res.send('A color id is required');
    }
    else {
        color_dal.edit(req.query.color_id, function(err, result){
            res.render('color/colorUpdate', {color: result[0][0], color: result[1]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.color_id == null) {
        res.send('A color id is required');
    }
    else {
        color_dal.getById(req.query.color_id, function(err, color){
            color_dal.getAll(function(err, color) {
                res.render('color/colorUpdate', {color: color[0], color: color});
            });
        });
    }

});

router.get('/update', function(req, res) {
    color_dal.update(req.query, function(err, result){
        res.redirect(302, '/color/all');
    });
});

// Delete a color for the given color_id
router.get('/delete', function(req, res){
    if(req.query.color_id == null) {
        res.send('color_id is null');
    }
    else {
        color_dal.delete(req.query.color_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/color/all');
            }
        });
    }
});

module.exports = router;

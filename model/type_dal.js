var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM type;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(type_id, callback) {
    var query = 'SELECT * FROM type WHERE type_id = ?';
    var queryData = [type_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE type
    var query = 'INSERT INTO type (type) VALUES (?)';

    var queryData = [params.type];

    connection.query(query, params.type, function(err, result) {

        // THEN USE THE type_id RETURNED AS insertId AND THE SELECTED ADDRESSes_IDs INTO type_address
        var type_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO type_address (type_id, addresses_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var typeAddressData = [];
        if (params.addresses_id.constructor === Array) {
            for (var i = 0; i < params.addresses_id.length; i++) {
                typeAddressData.push([type_id, params.addresses_id[i]]);
            }
        }
        else {
            typeAddressData.push([type_id, params.addresses_id]);
        }

        // NOTE THE EXTRA [] AROUND typeAddressData
        connection.query(query, [typeAddressData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(type_id, callback) {
    var query = 'DELETE FROM type WHERE type_id = ?';
    var queryData = [type_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var typeAddressInsert = function(type_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO type_address (type_id, addresses_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var typeAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            typeAddressData.push([type_id, addressIdArray[i]]);
        }
    }
    else {
        typeAddressData.push([type_id, addressIdArray]);
    }
    connection.query(query, [typeAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.typeAddressInsert = typeAddressInsert;

//declare the function so it can be used locally
var typeAddressDeleteAll = function(type_id, callback){
    var query = 'DELETE FROM type_address WHERE type_id = ?';
    var queryData = [type_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.typeAddressDeleteAll = typeAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE type SET type = ? WHERE type_id = ?';
    var queryData = [params.type, params.type_id];

    connection.query(query, queryData, function(err, result) {
        //delete type_address entries for this type
        typeAddressDeleteAll(params.type_id, function(err, result){

            if(params.addresses_id != null) {
                //insert type_address ids
                typeAddressInsert(params.type_id, params.addresses_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.edit = function(type_id, callback) {
    var query = 'CALL type_getinfo(?)';
    var queryData = [type_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.high_value = function(callback) {
    var query = 'select type, sum(cost * quantity) as total_stock from product ' +
        'join type on product.type_id = type.type_id ' +
        'group by product.type_id ' +
        'having total_stock > 5000 ' ;

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

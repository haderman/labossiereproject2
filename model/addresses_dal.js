var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM addresses;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function (addresses_id, callback) {
    var query = 'SELECT * FROM addresses WHERE addresses_id = ?';
    var queryData = [addresses_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    //Insert addresses
    var query = 'INSERT INTO addresses (street) VALUES (?)';
    var queryData = [params.street];
    connection.query(query, params.street, function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(addresses_id, callback) {
    var query = 'DELETE FROM addresses WHERE addresses_id = ?';
    var queryData = [addresses_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE addresses SET street = ? WHERE addresses_id = ?';
    var queryData = [params.street, params.addresses_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(addresses_id, callback) {
    var query = 'CALL addresses_getinfo(?)';
    var queryData = [addresses_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM sales;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(sale_id, callback) {
    var query = 'SELECT * FROM sales WHERE sale_id = ?';
    var queryData = [sale_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE sale
    var query = 'INSERT INTO sales (sale_date) VALUES (?)';

    var queryData = [params.sale_date];

    connection.query(query, params.sale_date, function(err, result) {

        // THEN USE THE sale_id RETURNED AS insertId AND THE SELECTED ADDRESSes_IDs INTO sale_address
        var sale_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO sale_address (sale_id, addresses_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var saleAddressData = [];
        if (params.addresses_id.constructor === Array) {
            for (var i = 0; i < params.addresses_id.length; i++) {
                saleAddressData.push([sale_id, params.addresses_id[i]]);
            }
        }
        else {
            saleAddressData.push([sale_id, params.addresses_id]);
        }

        // NOTE THE EXTRA [] AROUND saleAddressData
        connection.query(query, [saleAddressData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(sale_id, callback) {
    var query = 'DELETE FROM sales WHERE sale_id = ?';
    var queryData = [sale_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var saleAddressInsert = function(sale_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO sale_address (sale_id, addresses_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var saleAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            saleAddressData.push([sale_id, addressIdArray[i]]);
        }
    }
    else {
        saleAddressData.push([sale_id, addressIdArray]);
    }
    connection.query(query, [saleAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.saleAddressInsert = saleAddressInsert;

//declare the function so it can be used locally
var saleAddressDeleteAll = function(sale_id, callback){
    var query = 'DELETE FROM sale_address WHERE sale_id = ?';
    var queryData = [sale_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.saleAddressDeleteAll = saleAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE sales SET sale_date = ? WHERE sale_id = ?';
    var queryData = [params.sale_date, params.sale_id];

    connection.query(query, queryData, function(err, result) {
        //delete sale_address entries for this sale
        saleAddressDeleteAll(params.sale_id, function(err, result){

            if(params.addresses_id != null) {
                //insert sale_address ids
                saleAddressInsert(params.sale_id, params.addresses_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.edit = function(sale_id, callback) {
    var query = 'CALL sale_getinfo(?)';
    var queryData = [sale_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
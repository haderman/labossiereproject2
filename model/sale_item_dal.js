var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM sale_item;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(sale_item_id, callback) {
    var query = 'SELECT * FROM sale_item WHERE sale_item_id = ?';
    var queryData = [sale_item_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE sale_item
    var query = 'INSERT INTO sale_item (sale_id) VALUES (?)';

    var queryData = [params.sale_id];

    connection.query(query, params.sale_id, function(err, result) {

        // THEN USE THE sale_item_id RETURNED AS insertId AND THE SELECTED ADDRESSes_IDs INTO sale_item_address
        var sale_item_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO sale_item_address (sale_item_id, addresses_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var saleitemAddressData = [];
        if (params.addresses_id.constructor === Array) {
            for (var i = 0; i < params.addresses_id.length; i++) {
                saleitemAddressData.push([sale_item_id, params.addresses_id[i]]);
            }
        }
        else {
            saleitemAddressData.push([sale_item_id, params.addresses_id]);
        }

        // NOTE THE EXTRA [] AROUND saleitemAddressData
        connection.query(query, [saleitemAddressData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(sale_item_id, callback) {
    var query = 'DELETE FROM sale_item WHERE sale_item_id = ?';
    var queryData = [sale_item_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var saleitemAddressInsert = function(sale_item_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO sale_item_address (sale_item_id, addresses_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var saleitemAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            saleitemAddressData.push([sale_item_id, addressIdArray[i]]);
        }
    }
    else {
        saleitemAddressData.push([sale_item_id, addressIdArray]);
    }
    connection.query(query, [saleitemAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.saleitemAddressInsert = saleitemAddressInsert;

//declare the function so it can be used locally
var saleitemAddressDeleteAll = function(sale_item_id, callback){
    var query = 'DELETE FROM sale_item_address WHERE sale_item_id = ?';
    var queryData = [sale_item_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.saleitemAddressDeleteAll = saleitemAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE sale_item SET sale_id = ? WHERE sale_item_id = ?';
    var queryData = [params.sale_id, params.sale_item_id];

    connection.query(query, queryData, function(err, result) {
        //delete sale_item_address entries for this sale_item
        saleitemAddressDeleteAll(params.sale_item_id, function(err, result){

            if(params.addresses_id != null) {
                //insert sale_item_address ids
                saleitemAddressInsert(params.sale_item_id, params.addresses_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.edit = function(sale_item_id, callback) {
    var query = 'CALL sale_item_getinfo(?)';
    var queryData = [sale_item_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};